import { rendererService as renderer } from 'core/services/index';

export function App() {
    var self = this;

    self.init = function() {
      renderer.process(document.body);
    }
}
