export const routes = [
    {
        name: 'notes',
        path: '/notes',
        title: '_(Notes App)',
        template: require('./template.html')
    }
];
