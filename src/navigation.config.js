import { routes } from './routes.config';

export const config = {
    menu: [
        'dashboard',
        'apps'
    ],
    routes
};
