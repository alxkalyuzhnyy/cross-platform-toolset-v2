import { Component } from "core/classes";

@Component({
    selector: 'nav-menu',
    template: './nav-menu.component.html',
    styles: './nav-menu.component.scss'
})
export class NavMenuComponent {
    constructor() {
        this.navigation = [
            ['index.html', 'Home'],
            ['test-page.html', 'Test page'],
            ['?popup=test-page.html', 'Test page in popup'],
            ['performance-test.html', 'Performance test']
        ];
    }
}
