export const routes = [
    {
        name: 'root',
        path: '/',
        template: require('./templates/template.html'),
        children: [
            {
                name: 'apps',
                path: '/apps',
                title: '_(Apps list)',
                template: require('./templates/apps.html'),
                children: require('./pages')
            },
            {
                name: 'dashboard',
                path: '/dashboard',
                title: '_(Dashboard)',
                template: require('./templates/dashboard.html')
            },
            {
                path: '/',
                redirect: 'apps'
            }
        ]
    }
];
