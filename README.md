# Cross-platform Toolset v2 (WIP)

This is a work-in-progress toolset I use in day-to-day work.

Should use separate processes for logic and UI with text-serializable messaging protocol in between - this architecture allows to build solution for any of:
* Browser plugin
* Progressive web app
* Desktop app

A replacement for my old toolset:
![Toolset](toolset.jpeg)

**Current status:** early development stage
